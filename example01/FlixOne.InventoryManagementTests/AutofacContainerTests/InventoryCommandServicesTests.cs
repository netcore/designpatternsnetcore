﻿using System;
using Autofac;
using Autofac.Core;
using FlixOne.InventoryManagementTests.ImplemenationFactoryTests;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace FlixOne.InventoryManagementTests.AutofacContainerTests
{
    
    public class InventoryCommandServicesTests
    {
        IContainer Container { get; set; }

        public InventoryCommandServicesTests()
        {
            IServiceCollection services = new ServiceCollection();
            var builder = new ContainerBuilder();

            builder.RegisterType<QuitCommand>().Named<InventoryCommand>("q");
            builder.RegisterType<QuitCommand>().Named<InventoryCommand>("quit");
            builder.RegisterType<UpdateQuantityCommand>().Named<InventoryCommand>("u");
            builder.RegisterType<UpdateQuantityCommand>().Named<InventoryCommand>("updatequantity");
            builder.RegisterType<HelpCommand>().Named<InventoryCommand>("?");
            builder.RegisterType<AddInventoryCommand>().Named<InventoryCommand>("a");
            builder.RegisterType<AddInventoryCommand>().Named<InventoryCommand>("addinventory");
            builder.RegisterType<GetInventoryCommand>().Named<InventoryCommand>("g");
            builder.RegisterType<GetInventoryCommand>().Named<InventoryCommand>("getinventory");
            builder.RegisterType<UpdateQuantityCommand>().Named<InventoryCommand>("u");
            builder.RegisterType<UpdateQuantityCommand>().Named<InventoryCommand>("u");
            builder.RegisterType<UnknownCommand>().As<InventoryCommand>();

            Container = builder.Build();
        }

        public InventoryCommand GetCommand(string input)
        {
            return Container.ResolveOptionalNamed<InventoryCommand>(input.ToLower()) ?? Container.Resolve<InventoryCommand>();
        }

        [Fact]
        public void QuitCommand_Successfull()
        {
            Assert.IsType<QuitCommand>(GetCommand("q"));
            Assert.IsType<QuitCommand>(GetCommand("quit"));
        }

        [Fact]
        public void HelpCommand_Successful()
        {
            Assert.IsType<HelpCommand>(GetCommand("?"));
        }

        [Fact]
        public void UnknownCommand_Successful()
        {
            Assert.IsType<UnknownCommand>(GetCommand("add"));
            Assert.IsType<UnknownCommand>(GetCommand("addinventry"));
            Assert.IsType<UnknownCommand>(GetCommand("h"));
            Assert.IsType<UnknownCommand>(GetCommand("help"));
        }

        [Fact]
        public void AddInventoryCommand_Successful()
        {
            Assert.IsType<AddInventoryCommand>(GetCommand("a"));
            Assert.IsType<AddInventoryCommand>(GetCommand("addinventory"));
        }

        [Fact]
        public void GetInventoryCommand_Successful()
        {
            Assert.IsType<GetInventoryCommand>(GetCommand("g"));
            Assert.IsType<GetInventoryCommand>(GetCommand("getinventory"));
        }

        [Fact]
        public void UpdateQuantityCommand_Successful()
        {
            Assert.IsType<UpdateQuantityCommand>(GetCommand("u"));
            Assert.IsType<UpdateQuantityCommand>(GetCommand("updatequantity"));
            Assert.IsType<UpdateQuantityCommand>(GetCommand("UpdaTEQuantity"));
        }
    }
}

