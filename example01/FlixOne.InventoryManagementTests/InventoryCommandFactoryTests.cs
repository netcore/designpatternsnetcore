﻿using System;
using FlixOne.InventoryManagement.Command;
using Xunit;
using System.Collections.Generic;
using FlixOne.InventoryManagement.Repository;

namespace FlixOne.InventoryManagementTests
{
    public class InventoryCommandFactoryTests 
    {
        public InventoryCommandFactory Factory { get; set; }
        public InventoryContext Context { get; set; }

        public InventoryCommandFactoryTests()
        {
            var expectedInterface = new Helpers.TestUserInterface(
                new List<Tuple<string, string>>(),
                new List<string>(),
                new List<string>()
                );
            Context = new InventoryContext();
            Factory = new InventoryCommandFactory(expectedInterface, Context);
        }

        [Fact]
        public void QuitCommand_Successful()
        {
            Assert.IsType<QuitCommand>(Factory.GetCommand("q"));
            Assert.IsType<QuitCommand>(Factory.GetCommand("quit"));
        }

        [Fact]
        public void HelpCommand_Successful()
        {
            Assert.IsType<HelpCommand>(Factory.GetCommand("?"));
        }

        [Fact]
        public void UnknownCommand_Successful()
        {
            Assert.IsType<UnknownCommand>(Factory.GetCommand("add"));
            Assert.IsType<UnknownCommand>(Factory.GetCommand("addinventry"));
            Assert.IsType<UnknownCommand>(Factory.GetCommand("h"));
            Assert.IsType<UnknownCommand>(Factory.GetCommand("help"));
        }

        [Fact]
        public void UpdateQuantityCommand_Successful()
        {
            Assert.IsType<UpdateQuantityCommand>(Factory.GetCommand("u"));
            Assert.IsType<UpdateQuantityCommand>(Factory.GetCommand("updatequantity"));
            Assert.IsType<UpdateQuantityCommand>(Factory.GetCommand("UpdaTEQuantity"));
        }
    }
}

