﻿using System;
using System.Collections.Generic;
using FlixOne.InventoryManagement.Command;
using Xunit;

namespace FlixOne.InventoryManagementTests
{
    public class QuitCommandTests
    {
        //[Fact(Skip = "QuitCommandTests_Successful has not been implemented.")]
        [Fact]
        public void QuitCommandTests_Successful()
        {
            var expectedInterface = new Helpers.TestUserInterface(
                new List<Tuple<string, string>>(),//ReadValue()
                new List<string> {
                    "Thank you for using FlixOne Inventory Management System"
                },//WriteMessage()
                new List<string>()//WriteWarning()
                );
            var command = new QuitCommand(expectedInterface);
            var result = command.RunCommand();
            expectedInterface.Validate();

            Assert.True(result.shouldQuit, "Quit is a terminating command.");
            Assert.True(result.wasSuccessful, "Quit did not complete Successfully.");
        }
    }
}

