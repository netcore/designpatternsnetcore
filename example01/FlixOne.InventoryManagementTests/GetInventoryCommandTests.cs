﻿using System;
using System.Collections.Generic;
using System.Text;
using FlixOne.InventoryManagement.Command;
using FlixOne.InventoryManagement.Models;
using FlixOne.InventoryManagementTests.Helpers;
using Xunit;

namespace FlixOne.InventoryManagementTests
{
    public class GetInventoryCommandTests
    {
        [Fact]
        public void GetInventoryCommand_Successful()
        {
            var expectedInterface = new Helpers.TestUserInterface(
                new List<Tuple<string, string>>(),
                new List<string>
                {
                    $"{"Gremlins",-30}\tQuantity:7",
                    $"{"Willowsong",-30}\tQuantity:3"
                },
                new List<string>()
                );

            var context = new TestInventoryContext(new Dictionary<string, Book>
            {
                {"Gremlins", new Book{Id = 1, Name = "Gremlins", Quantity = 7 } },
                {"Willowsong", new Book{Id = 2, Name = "Willowsong", Quantity = 3 } }
            });

            var command = new GetInventoryCommand(expectedInterface, context);
            var result = command.RunCommand();

            Assert.False(result.shouldQuit, "GetInventory is not a terminating command.");
            Assert.True(result.wasSuccessful, "GetInventory did not complete Successfully.");

            Assert.Empty(context.GetAddedBooks());
            Assert.Empty(context.GetUpdatedBooks());
        }
    }
}

