﻿using System;
using System.Collections.Generic;
using FlixOne.InventoryManagement.Command;
using FlixOne.InventoryManagement.Models;
using FlixOne.InventoryManagementTests.Helpers;
using Xunit;

namespace FlixOne.InventoryManagementTests
{
    public class UpdateQuantityCommandTests
    {
        [Fact]
        public void UpdateQuantity_ExistingBook_Successful()
        {
            const string expectedBookName = "UpdateQuantityUnitTest";
            var expectedInterface = new Helpers.TestUserInterface(
                new List<Tuple<string, string>>
                {
                    new Tuple<string, string>("Enter name:", expectedBookName),
                    new Tuple<string, string>("Enter quantity:", "6")
                },
                new List<string>(),
                new List<string>()
                );
            var context = new TestInventoryContext(new Dictionary<string, Book>
            {
                {"Beavers", new Book{Id = 1, Name ="Beavers", Quantity = 3} },
                {expectedBookName, new Book{Id = 2, Name = expectedBookName, Quantity = 7} },
                {"Ducks", new Book{Id = 3, Name = "Ducks", Quantity = 12} }
            });

            var command = new UpdateQuantityCommand(expectedInterface, context);
            var result = command.RunCommand();

            Assert.False(result.shouldQuit, "UpdateQuantity is not a terminating command.");
            Assert.True(result.wasSuccessful, "UpdateQuantity did not complete Successfully.");

            Assert.Empty(context.GetAddedBooks());
            var updateBooks = context.GetUpdatedBooks();
            Assert.Single(updateBooks);
        }
    }
}

