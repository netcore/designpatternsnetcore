﻿using System;
using Xunit;
using System.Collections.Generic;
using FlixOne.InventoryManagement.Command;
using FlixOne.InventoryManagement.Models;
using FlixOne.InventoryManagement.Repository;
using FlixOne.InventoryManagementTests.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace FlixOne.InventoryManagementTests
{
    public class InventoryCommandFunctionTests
    {
        ServiceProvider Services { get; set; }

        public InventoryCommandFunctionTests()
        {
            var expectedInterface = new Helpers.TestUserInterface(
                new List<Tuple<string, string>>(),
                new List<string>(),
                new List<string>()
                );
            IServiceCollection services = new ServiceCollection();
            services.AddSingleton<IInventoryContext, InventoryContext>();
            services.AddTransient<Func<string, InventoryCommand>>(InventoryCommand.GetInventoryCommand);
            Services = services.BuildServiceProvider();
        }

        [Fact]
        public void QuitCommand_Successfull()
        {
            Assert.IsType<QuitCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("q"));
            Assert.IsType<QuitCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("quit"));
        }

        [Fact]
        public void HelpCommand_Successful()
        {
            Assert.IsType<HelpCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("?"));
        }

        [Fact]
        public void UnknownCommand_Successful()
        {
            Assert.IsType<UnknownCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("add"));
            Assert.IsType<UnknownCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("addinventry"));
            Assert.IsType<UnknownCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("h"));
            Assert.IsType<UnknownCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("help"));
        }

        [Fact]
        public void AddInventoryCommand_Successful()
        {
            Assert.IsType<AddInventoryCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("a"));
            Assert.IsType<AddInventoryCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("addinventory"));
        }

        [Fact]
        public void GetInventoryCommand_Successful()
        {
            Assert.IsType<GetInventoryCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("g"));
            Assert.IsType<GetInventoryCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("getinventory"));
        }

        [Fact]
        public void UpdateQuantityCommand_Successful()
        {
            Assert.IsType<UpdateQuantityCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("u"));
            Assert.IsType<UpdateQuantityCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("updatequantity"));
            Assert.IsType<UpdateQuantityCommand>(Services.GetService<Func<string, InventoryCommand>>().Invoke("UpdaTEQuantity"));
        }
    }
}

