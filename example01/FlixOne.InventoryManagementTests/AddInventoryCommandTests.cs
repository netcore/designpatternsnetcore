using Xunit;
using System.Linq;
using System.Collections.Generic;
using System;
using FlixOne.InventoryManagementTests.Helpers;
using FlixOne.InventoryManagement.Models;
using FlixOne.InventoryManagement.Command;

namespace FlixOne.InventoryManagementTests;

public class AddInventoryCommandTests
{
    [Fact]
    public void AddInventoryCommand_Successful()
    {
        const string expectedBookName = "AddInventoryUnitTest";
        var expectedInterface = new Helpers.TestUserInterface(
            new List<Tuple<string, string>>
            {
                new Tuple<string, string>("Enter name:", expectedBookName)
            },
            new List<string>(),
            new List<string>()
            );
        var context = new TestInventoryContext(new Dictionary<string, Book>
        {
            {"Gremlins", new Book{Id =1, Name = "Gremlins", Quantity = 7} }
        });
        //создаем экземпляр команды
        var command = new AddInventoryCommand(expectedInterface, context);
        var result = command.RunCommand();

        Assert.False(result.shouldQuit, "AddInventory is not a terminating command.");
        Assert.True(result.wasSuccessful, "AddInventory did not complete Successfully.");

        //убеждаемся, что была добавлена книга с заданным именем и количеством 0
        Assert.Single(context.GetAddedBooks());
        var newBook = context.GetAddedBooks().First();
        Assert.Equal(expectedBookName, newBook.Name);

    }
}