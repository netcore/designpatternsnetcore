﻿using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlixOne.InventoryManagement.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace FlixOne.InventoryManagementTests
{
    public class InventoryContextTests
    {
        ServiceProvider Services { get; set; }

        public InventoryContextTests() {
            IServiceCollection services = new ServiceCollection();
            var context = new InventoryContext();
            services.AddSingleton<IInventoryContext, InventoryContext>();
            Services = services.BuildServiceProvider();
        }

        [Fact]
        public void MaintainBooks_Successful()
        {
            List<Task> tasks = new List<Task>();

            //добавление 30 книг
            foreach(var id in Enumerable.Range(1,30))
            {
                //context.AddBook($"Book_{id}");
                tasks.Add(AddBook($"Book_{id}"));
            }
            Task.WaitAll(tasks.ToArray());
            tasks.Clear();

            //обновим количество книг, добавив 1, 2, 3, 4, 5, ...
            foreach(var quantity in Enumerable.Range(1, 10))
            {
                foreach (var id in Enumerable.Range(1,30))
                {
                    //context.UpdateQuantity($"Book_{id}", quantity);
                    tasks.Add(UpdateQuantity($"Book_{id}", quantity));
                }
            }
           

            //обновим количество книг, вычтим 1, 2, 3, 4, 5, ...
            foreach (var quantity in Enumerable.Range(1, 10))
            {
                foreach (var id in Enumerable.Range(1, 30))
                {
                    //context.UpdateQuantity($"Book_{id}", -quantity);
                    tasks.Add(UpdateQuantity($"Book_{id}", -quantity));
                }
            }

            Task.WaitAll(tasks.ToArray());

            foreach (var book in Services.GetService<IInventoryContext>().GetBooks())
            {
                Assert.Equal(0, book.Quantity);
            }
        }

        public Task AddBook(string book)
        {
            return Task.Run(() =>
            {
                //var context = new InventoryContext();
                Assert.True(Services.GetService<IInventoryContext>().AddBook(book));
            });
        }

        public Task UpdateQuantity(string book, int quantity)
        {
            return Task.Run(() =>
            {
                //var context = new InventoryContext();
                Assert.True(Services.GetService<IInventoryContext>().UpdateQuantity(book, quantity));
            });
        }
    }
}

