﻿using FlixOne.InventoryManagement.Command;
using FlixOne.InventoryManagement.Repository;
using FlixOne.InventoryManagement.UserInterface;
using FlixOne.InventoryManagementClient;
using Microsoft.Extensions.DependencyInjection;

IServiceCollection services = new ServiceCollection();
ConfigureServices(services);
IServiceProvider serviceProvider = services.BuildServiceProvider();
var service = serviceProvider.GetService<ICatalogService>();
service.Run();
Console.WriteLine("CatalogService has completed.");
Console.ReadLine();

static void ConfigureServices(IServiceCollection services)
{
    services.AddTransient<IUserInterface, ConsoleUserInterface>();
    services.AddTransient<ICatalogService, CatalogService>();
    //services.AddTransient<IInventoryCommandFactory, InventoryCommandFactory>();

    services.AddTransient<Func<string, InventoryCommand>>(InventoryCommand.GetInventoryCommand);

    //для всех трех интерфейсов используется один и тот же экземпляр InventoryContext
    var context = new InventoryContext();
    services.AddSingleton<IInventoryReadContext, InventoryContext>(p => context);
    services.AddSingleton<IInventoryWriteContext, InventoryContext>(p => context);
    services.AddSingleton<IInventoryContext, InventoryContext>(p => context);
}