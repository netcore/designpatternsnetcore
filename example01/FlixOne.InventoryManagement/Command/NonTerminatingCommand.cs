﻿using System;
using FlixOne.InventoryManagement.UserInterface;

namespace FlixOne.InventoryManagement.Command
{
    public abstract class NonTerminatingCommand: InventoryCommand
    {
        public NonTerminatingCommand(IUserInterface userInterface): base(commandIsTerminating: false, userInterface: userInterface)
        {
        }
    }
}

